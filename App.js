
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import RootStackScreen from './src/routes/RootStackScreen';


function App() {
  return (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );
}

export default App;
