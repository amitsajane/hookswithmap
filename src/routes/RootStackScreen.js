/* eslint-disable prettier/prettier */
import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import nextScreen from '../screens/nextScreen';
import Home from '../screens/Home';
import locationScreen from '../screens/locationScreen'
import mapSreen from '../screens/mapSreen'
import FavouriteArea from '../screens/FavouriteArea'
import ShareLocation from '../screens/ShareLocation'


const RootStack = createStackNavigator();

const RootStackScreen = ({ navigation }) => (
    <RootStack.Navigator headerMode='none'>

        <RootStack.Screen name="ShareLocation" component={ShareLocation} />
        <RootStack.Screen name="FavouriteArea" component={FavouriteArea} />
        <RootStack.Screen name="mapSreen" component={mapSreen} />
        <RootStack.Screen name="nextScreen" component={nextScreen} />

        <RootStack.Screen name="Home" component={Home} />

        <RootStack.Screen name="locationScreen" component={locationScreen} />


    </RootStack.Navigator>
);

export default RootStackScreen;