import React, { useState } from "react";
import Geolocation from "@react-native-community/geolocation";
import { Button, View, Text } from "react-native";

const mapSreen = () => {
  const [error, setError] = useState("");
  const [position, setPosition] = useState({
    latitude: 0,
    longitude: 0
  });

  const getPosition = () => {
    // Geolocation.getCurrentPosition(
    //   pos => {
    //     setError("");
    //     setPosition({
    //       latitude: pos.coords.latitude,
    //       longitude: pos.coords.longitude
    //     });
    //   },
    //   e => setError(e.message)
    // );
    Geolocation.getCurrentPosition(
        (position) => {
          const initialPosition = JSON.stringify('>>>>' + position.coords);
          console.log(JSON.stringify('>>>>' + position.coords.latitude));
          console.log(JSON.stringify('>>>>' + position.coords.longitude));

          this.setState({
            region: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121,
            },
          });
        },
        (error) => console.log('Error', JSON.stringify(error)),
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
      );
  };

  return (
    <View>
      <Button title="Get Current Position" onPress={getPosition} />
      {error ? (
        <Text>Error retrieving current position</Text>
      ) : (
        <>
          <Text>Latitude: {position.latitude}</Text>
          <Text>Longitude: {position.longitude}</Text>
        </>
      )}
    </View>
  );
};

export default mapSreen;