/* eslint-disable no-unused-vars */
/* eslint-disable eqeqeq */
/* eslint-disable react-native/no-inline-styles */
import React, {Component, Fragment} from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  PermissionsAndroid,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

import MapView, {
  Callout,
  Marker,
  ProviderPropType,
  Circle,
} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';


let {width, height} = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.001;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;

function log(eventName, e) {
  console.log(eventName, e.nativeEvent);
}

export default class Home extends Component {
  constructor() {
    super();
    this.state = {
      region: {
        latitude: 0,
        longitude: 0,
        // latitudeDelta: 0.015,
        // longitudeDelta: 0.0121,
        initialPosition: 'unknown',
        lastPosition: 'unknown',
      },
      polygons: [
        {latitude: 37.421998333333335, longitude: -118.08400000000002},
        {latitude: 38.421998333333335, longitude: -119.08400000000002},
        {latitude: 39.421998333333335, longitude: -120.08400000000002},
        {latitude: 36.421998333333335, longitude: -121.08400000000002},
      ],
      location: '',
      getlanglat: '',
    };
  }
  watchID: ? number = null;

  async componentDidMount() {
    PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    ).then((granted) => {
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Geolocation.getCurrentPosition(
          (position) => {
            const initialPosition = JSON.stringify('>>>>' + position.coords);
            console.log(JSON.stringify('>>>>' + position.coords.latitude));
            console.log(JSON.stringify('>>>>' + position.coords.longitude));

            this.setState({
              region: {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
              },
            });
          },
          (error) => console.log('Error', JSON.stringify(error)),
          {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
        );
        this.watchID = Geolocation.watchPosition((position) => {
          // const lastPosition = JSON.stringify(position.coords);

          this.setState({
            region: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              // latitudeDelta: 0.015,
              // longitudeDelta: 0.0121,
            },
          });
        });
      }
    });
  }
  onRegionChange(region) {
    this.setState({
      region: region,
    });
  }

  render() {
    return (
      <View style={{width: '100%', flex: 1, alignItems: 'center'}}>
        <View
          // eslint-disable-next-line react-native/no-inline-styles
          style={{
            backgroundColor: '#fff',
            width: '100%',
            alignItems: 'flex-start',
            padding: 10,
          }}>
          <Text
            // eslint-disable-next-line react-native/no-inline-styles
            style={{
              fontSize: 25,
              color: '#111111',
              fontWeight: 'bold',
              marginTop: 22,
            }}>
            Favourite area
          </Text>
          <Text
            // eslint-disable-next-line react-native/no-inline-styles
            style={{
              fontSize: 20,
              color: '#404040',
              fontWeight: '800',
              textAlign: 'center',
              marginTop: 10,
            }}>
            Drag the map to your favourite area
          </Text>
        </View>
        {this.state.region.latitude != 0 ? (
          <MapView
            tracksViewChanges={false}
            showsUserLocation={true}
            style={styles.map}
            initialRegion={this.state.region}
            onRegionChange={(region) => {
              this.setState({region});
            }}>
            {/* <Marker
                coordinate={this.state.region}
                >

              </Marker> */}

            <MapView.Circle
              center={this.state.region}
              radius={500}
              meterLimit={5000}
              strokeWidth={2}
              strokeColor="#3399ff"
              fillColor="#ffff"
            />
            <MapView.Polygon
              coordinates={this.state.polygons}
              fillColor="rgba(0, 200, 0, 0.5)"
              strokeColor="rgba(0,0,0,0.5)"
              strokeWidth={2}
            />
          </MapView>
        ) : null}

        <View
          style={{
            width: '100%',
            position: 'absolute',
            bottom: 20,
            padding: 15,
          }}>
          <TouchableOpacity
            style={{
              width: '100%',
              backgroundColor: '#FC8B8B',
              padding: 20,
              height: 50,
              padding: 10,
              borderRadius: 5,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 16, color: '#fff', fontWeight: 'bold'}}>
              Select this area
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    height: '100%',
    width: '100%',
  },
});
