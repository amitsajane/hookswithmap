/* eslint-disable no-undef */
/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState, useRef } from 'react';
import { AppState, View, Text, StyleSheet, Image, PermissionsAndroid, Button, TouchableOpacity, SafeAreaView } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const nextScreen = () => {
  const [permisson, setPermisson] = useState(PermissionsAndroid.RESULTS.GRANTED);


  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  const navigation = useNavigation();
  useEffect(() => {
    
    AppState.addEventListener('change', _handleAppStateChange);

    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
    };
  }, []);

  async function requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        navigation.navigate('Home');
        // console.log('You can use the location');

      } else {
        setPermisson(!permisson)
        console.log('location permission denied');
        //  alert('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  const _handleAppStateChange = (nextAppState) => {
    if (appState.current.match(/inactive|background/) &&
      nextAppState === 'active' &&  setPermisson(permisson)  ) {
        // permisson(setPermisson)
      console.log('permission allowed')
      console.log('granted', PermissionsAndroid.RESULTS)
        setPermisson(PermissionsAndroid.RESULTS.GRANTED)
      
      console.log('App has come to the foreground!');
    }
    else {
      // alert("jjjjj")
    }

    appState.current = nextAppState;
    setAppStateVisible(appState.current);
    console.log('AppState', appState.current);
  };


  return (
    <SafeAreaView style={{ flex: 1 }}>
      {console.log("=====>>>>>", permisson)}

      {permisson ?

        <View style={{ alignItems: 'center', justifyContent: "center" }}>
          <Image
            source={{
              uri:
                'https://raw.githubusercontent.com/AboutReact/sampleresource/master/location.png',
            }}
            style={{ width: 100, height: 100, marginTop: 30 }}
          />

          <Text style={{ fontSize: 25, color: '#111111', fontWeight: 'bold', marginTop: 20 }}>Share Location</Text>
          <Text style={{ fontSize: 18, color: '#404040', fontWeight: '600', textAlign: 'center', marginTop: 10 }}>You can't use the app if you do not allow location services </Text>

          <View style={{ width: "100%", padding: 20 }}>
            <TouchableOpacity style={{ width: '100%', backgroundColor: "#FC8B8B", padding: 20, height: 50, padding: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}

              onPress={requestLocationPermission}
            >
              <Text style={{ fontSize: 18, color: "#fff", fontWeight: 'bold' }}>Next</Text>
            </TouchableOpacity>
          </View>
        </View>

        :
        <View
      style={{
        width: '100%',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
      }}>
      <Image
        source={{
          uri:
            'https://raw.githubusercontent.com/AboutReact/sampleresource/master/location.png',
        }}
        style={{width: 100, height: 100, marginTop: 30}}
      />
      <Text
        style={{
          fontSize: 25,
          color: '#111111',
          fontWeight: 'bold',
          marginTop: 22,
        }}>
        Oh no!
      </Text>
      <Text
        style={{
          fontSize: 20,
          color: '#404040',
          fontWeight: '800',
          textAlign: 'center',
          marginTop: 10,
        }}>
        Zazu can't be used without allowing location services
      </Text>
      <Text
        style={{
          fontSize: 20,
          color: '#404040',
          fontWeight: '800',
          textAlign: 'center',
          marginTop: 10,
        }}>
        Go to You phones:
      </Text>
      <Text
        style={{
          fontSize: 20,
          color: '#404040',
          fontWeight: '800',
          textAlign: 'center',
          marginTop: 10,
        }}>
        Settings-> Zazu -> Location and chose allow
      </Text>
    </View>
      }
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default nextScreen;
