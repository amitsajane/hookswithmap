/* eslint-disable prettier/prettier */
import { StyleSheet } from "react-native";
// import { colors } from "./colors";

export default appStyles = StyleSheet.create({
    montserratRegular: {
        fontFamily: 'greycliffcf-bold'
    },
    montserratBold: {
        fontFamily: 'greycliffcf-demibold'
    },
    openSansMeduim: {
        fontFamily: 'greycliffcf-medium'
    },
    openSansSemiheavy: {
        fontFamily: 'greycliffcf-heavy'
    },
    openSansExtraBold: {
        fontFamily: 'greycliffcf-extrabold'
    },
    openSansLight: {
        fontFamily: 'greycliffcf-light'
    },
    openSansLight: {
        fontFamily: 'greycliffcf-regular'
    },
  fontStyle:{
    fontFamily: 'greycliffcf-bold'
  },
  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
   // backgroundColor: "#fff"
  },
    divider: {
        backgroundColor: 'red'
    },
    //common used css
    lottie: {
        width: '100%',
        height: '100%',
    //   backgroundColor:"#FFF"
      },
      loader: {
        width: '100%',
        height: '100%',
     //  backgroundColor:"#fff"
      },
commonView:{
    flex:1,margin:15,bottom:10
},
  
  
  
  
  
    container: {
        flex: 1,
       // margin: 2,
       backgroundColor:'#fafafa',
      //  elevation:0
    },
    hederFont: {
        fontSize: 19, fontFamily:'greycliffcf-bold',
    },
    screenSubtitle: {
        fontSize: 24, 
        //top: 2

    },
    txtbeforeTextInput: {
        fontSize: 18, color: '#6C7596', fontWeight: '600', color: 'black', top: 20, left: 10

    },
    headerStartText: {
        fontSize: 16, color: '#FC8D8D', fontFamily:'greycliffcf-bold',marginLeft:6,
    },
    headerskipText: {
        fontSize: 16, color: '#FC8D8D',  fontFamily:'greycliffcf-bold'
    },
    headerBackButton: {
        height: 22, width: 22, tintColor: '#FC8D8D',top:1
    },
    marSubstitle: {
        //marginTop: 5, margin: 10
    },
    textInput: {
        height: 56, borderWidth: 1, borderRadius: 8, backgroundColor: 'white', borderColor: 'rgb(235,235,235)', fontSize: 18,fontFamily:'greycliffcf-demibold'
    },
    //intro screen
    logo: {
        justifyContent: 'center', alignItems: 'center'
    },
    btnContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin:10

    },
    logoImg: {
        width: '20%', height: 30, resizeMode: 'contain', tintColor: '#101654', top: 15,
    },
    SelectStartText: {
        justifyContent: 'center', alignItems: 'center',
        top: 150,
        fontWeight: "bold"
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: 1,
        top: 100,

        borderColor: '#EFEFEF',
        height: 54,
        borderRadius: 16,
     //   width: '80%',
       // margin: 10,
    },

    ImageStyle: {
        padding: 16,
        margin: 5,
        height: 22,
        width: 22,
        resizeMode: 'stretch',
        tinColor:'#FC8D8D',
        alignItems: 'center',
    },

    video: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
      },
    textColor: {
        color: '#FC8D8D',
        fontSize: 18,
        textAlign:'left',
        fontFamily:'greycliffcf-demibold',
       // fontWeight:'bold'
    },
    textBottom: {
        //  position: 'absolute',
        bottom: 8,
        justifyContent: 'center', alignItems: 'center',
    },

    //intro screen
    //header file
    navBar: {
        //elevation:4,
        shadowColor: 'transparent',
        height: 60,
        borderColor:'#fff',
        //   borderColor:'#fafafa',
        paddingTop:13,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',        
    },
    leftContainer: {
        flex: 1,
        flexDirection: 'row',
        left: 5,
        justifyContent: 'flex-start',
        alignItems:'center',
        //  backgroundColor: 'green'
    },
    rightContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        // top:10
        //backgroundColor: 'red',
    },
    rightIcon: {
        height: 10,
        width: 10,
        resizeMode: 'contain',
        //  backgroundColor: 'white',
    },

    termNcondtionBtn: {

        flexDirection: 'row',
       
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: 1,
        position: 'relative',
        // borderColor: '#EFEFEF',
        borderRadius: 27,
        width: '100%',
       
        // margin:20,
        // left:10,
        right: 0
        //margin: 20,

    },
    btnAccpetItem: {
        width: '100%',
        height: 57,
        backgroundColor: '#FC8B8B',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        // position: 'absolute',
        marginBottom:59
    },

    //namenage
    namenageSubtitle: {
        flex:1,margin:24

    },
    nameAgeText: {
        marginLeft: 40, top: 20
    },
    txtName: {
        fontSize: 18, color: '#6C7596',  color: 'black', top: 40, left: 10
    },
    txtAge: {
        height: 54, backgroundColor: 'white', borderRadius: 8, top: 10, borderWidth: 2, top: 30, borderColor: 'white'
    },
    inputAge: {
        height: 56, marginTop: 10,
    },
    btnText: {
         fontSize: 20, color: 'white'
    },

    //about you
    genderBtn: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: 1,
        top: 50,
        // borderColor: '#EFEFEF',
        height: 70,
        borderRadius: 210,
        width: '100%',
    },
    genderHBtn: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:16,
        borderWidth: 1,
        // borderColor: '#EFEFEF',
        height: 54,
        borderRadius: 27,
        width: '100%',
    },
    // looking for

    genderRangeStyleView: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: 4,
        top: 50,
        //borderRadius: 5,
       borderColor: '#fff',
        height: 108,
        borderRadius: 16,
        width: '100%',
        margin: 10,
    },
    containerRange: {
        flex: 1,
        justifyContent:'center',
        alignItems:'center'
    //  /   marginHorizontal: 15,
        //justifyContent: 'center',
      //  margin: '1%'
    },
    ageText: {
        fontSize: 20
    },

    //favourpage
    MainContainer: {
        position: 'absolute',
        height: 200,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        // alignItems: 'center',  
        //justifyContent: 'flex-end',  
    },
    mapStyle: {
        width:'100%',
        height: 280,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // right: 0,  
        display:'flex'
    },
    btnSetItem: {
        width: '100%',
        height: 50,
        // flex:1,
        backgroundColor: '#FC8B8B',
        // margin:10,
        // marginRight:40,
        // justifyContent: 'center', 
        // marginLeft:50,
        //marginRight:50,
        // alignItems: 'center',
        borderRadius: 20,
        // position: 'absolute',
        bottom: 10,
        marginHorizontal: 25,
        //justifyContent: 'center',
        right: 10,
        left: 10

    },
    //discription
    textAreaContainer: {
        borderColor: 'white',
        borderWidth: 1,
        borderRadius: 8,
        backgroundColor: 'white',

        // padding: 5,
        top: 20
    },
    textArea: {
        height: 136,
        //        textAlign: 'center',
        fontSize: 18,
      //  fontWeight: 'bold',
        fontFamily:'greycliffcf-demibold',
        //fontFamily:''
        //:'white',
        top: -30,
      
        justifyContent: "flex-start"
    },
    //interset
    containerApp: {
        //backgroundColor: 'blue', 
        flex: 1,
        margin:10,
        borderTopStartRadius: 20, borderTopEndRadius: 20,
        bottom:10,
        borderColor: '#EFEFEF',
    },        

    contentTitle: {
        fontSize: 28, 
        marginVertical: 10,
        textAlign: 'center',
        //fontFamily: fonts.bold
    },
    containerBlue: {
        flex: 1,
        // backgroundColor: 'blue',
        width: '100%',
    },
    p10: {
        padding: 20,justifyContent:'center'
    },
    center: {
        justifyContent: 'center', alignItems: 'center'
    },
    chipSelected: {
        // fontFamily: fonts.medium,
        borderWidth: 1, borderRadius: 20, padding: 10, paddingHorizontal: 8,
        margin: 4, color: 'white', backgroundColor: '#FC8B8B'
    },
    chipUnselected: {
        // fontFamily: fonts.medium, 
        borderColor: '#fafafa',
        borderWidth: 1, borderRadius: 30,
        //left:10,
        margin: 8,
    },
    //where fo you work
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: .5,
        top:20,
        borderColor: 'black',
        height: 54,
        width:'100%',
        borderRadius: 8,
        margin: 2
    },

    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center',
    tintColor:'#FC8D8D'
    },
    //personalinfo
    ImageStyleIcon: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        justifyContent:'flex-start',
      //  alignItems: 'center',
    tintColor:'#FC8D8D'
    },
    
    containerProducts: {
        paddingTop: 0,
        paddingLeft: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        },
        inputIOS: {
            fontSize: 16,
            paddingVertical: 12,
            paddingHorizontal: 10,
            borderWidth: 1,
            borderColor: 'gray',
            borderRadius: 4,
            color: 'black',
            paddingRight: 30, // to ensure the text is never behind the icon
          },
          inputAndroid: {
            fontSize: 16,
            paddingHorizontal: 1,
            paddingVertical: 8,
            borderWidth: 0.5,
            borderColor: 'purple',
            backgroundColor:'#000',
            borderRadius: 8,
            color: '#000',
            paddingRight: 10, // to ensure the text is never behind the icon
          },

          crossImage: {
            height: 12,
            width: 12,
            padding: 0,
            margin: 5,
            resizeMode:'contain',
            tintColor: '#000000'
        },
})