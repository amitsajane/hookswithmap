/* eslint-disable react-native/no-inline-styles */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useRef, useState, useEffect} from 'react';
import {AppState, StyleSheet, Text, View, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const locationScreen = ({route}) => {
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  const navigation = useNavigation();
  useEffect(() => {
    alert('paramKey')
    AppState.addEventListener('change', _handleAppStateChange);

    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
    };
  }, []);

  const _handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      // navigation.navigate('ShareLocation');
      console.log('App has come to the foreground!');
    }

    appState.current = nextAppState;
    setAppStateVisible(appState.current);
    console.log('AppState', appState.current);
  };
  return (
    <View
      style={{
        width: '100%',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
      }}>
      <Image
        source={{
          uri:
            'https://raw.githubusercontent.com/AboutReact/sampleresource/master/location.png',
        }}
        style={{width: 100, height: 100, marginTop: 30}}
      />
      <Text
        style={{
          fontSize: 25,
          color: '#111111',
          fontWeight: 'bold',
          marginTop: 22,
        }}>
        Oh no!
      </Text>
      <Text
        style={{
          fontSize: 20,
          color: '#404040',
          fontWeight: '800',
          textAlign: 'center',
          marginTop: 10,
        }}>
        Zazu can't be used without allowing location services
      </Text>
      <Text
        style={{
          fontSize: 20,
          color: '#404040',
          fontWeight: '800',
          textAlign: 'center',
          marginTop: 10,
        }}>
        Go to You phones:
      </Text>
      <Text
        style={{
          fontSize: 20,
          color: '#404040',
          fontWeight: '800',
          textAlign: 'center',
          marginTop: 10,
        }}>
        Settings-> Zazu -> Location and chose allow
      </Text>
    </View>
  );
};

export default locationScreen;
