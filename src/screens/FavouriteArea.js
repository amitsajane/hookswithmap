/* eslint-disable no-shadow */
/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  AsyncStorage,
  BackHandler, DeviceEventEmitter, Linking, Platform, Dimensions
} from 'react-native';

 // the variable name is arbitrary since it's exported as default
import appStyles from './appStyles'
import * as Animatable from 'react-native-animatable';
import { useNavigation } from '@react-navigation/native';
import MapView from 'react-native-maps';
import AnimatedLoader from "react-native-animated-loader";
import Geolocation from '@react-native-community/geolocation';
import OpenSettings from 'react-native-open-settings';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

export default function favouritearea() {
  /**Variables Defined */
  var initialRegion = {
    latitude: 0,
    longitude: 0,
    latitudeDelta: 0.92,
    longitudeDelta: 0.42,
  }
  const navigation = useNavigation();
  const [isMapReady] = useState(false);
  const [loading, showLoader] = useState(false);
  const [store, setStore] = useState(false);

  const [currentlocation, setLocation] = useState({ latitude: "", longitude: "", latitudeDelta: "", longitudeDelta: "" });
  const [lati, setLat] = useState(0);
  const [lng, setLng] = useState(0);
  // const [marker, setMarker] = useState(GLOBALS.markerArray)
  const [region, setRegion] = useState(initialRegion)
  const [mapInsize, setMap] = useState(false)
  useEffect(() => {
    /**Local Storage */
    //alert('sss')
    showLoader(true)
    // AsyncStorage.getItem('user').then((userinfo) => {
    //   var data = JSON.parse(userinfo)
    //   setStore(data)
    // })
    setTimeout(() => {

      showLoader(false)

    }, 4000);
    // if (GLOBALS.SignupData.the_latitude != undefined && GLOBALS.SignupData.the_latitude == !'') {
    //   var initialRegion = {
    //     latitude: GLOBALS.SignupData.the_latitude,
    //     longitude: GLOBALS.SignupData.the_lngtude,
    //     latitudeDelta: 0.91,
    //     longitudeDelta: 0.42,
    //   }
    //   setRegion(initialRegion)
    //   setMap(true)
    // }
    // setMarker(GLOBALS.markerArray)
    // GLOBALS.LATLNG = {
    //   latitude: 0,
    //   longitude:0
    // }
    Geolocation.getCurrentPosition((info) => {


      const long = info.coords.longitude
      const lat = info.coords.latitude
      setLat(lat)
      setLng(long)
      var initialRegion = {
        latitude: lat,
        longitude: long,
        latitudeDelta: 0.91,
        longitudeDelta: 0.42,
      }
      // GLOBALS.SignupData.the_latitude = lat
      // GLOBALS.SignupData.the_lngtude = long
      setRegion(initialRegion)
      if (long != null && long != '') {
        setTimeout(() => {
          setMap(true)

          showLoader(false)

        }, 500);
      }
    },
      (error) => getLocationDnied(),
      { enableHighAccuracy: true, timeout: 20000 });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const getLocationDnied = () => {
    showLoader(false)
    if (Platform.OS === 'android') {
      OpenSettings.openSettings()

    }
    // Linking.openSettings()
    navigation.navigate('sharelocation')
  }
 
  const onRegionChange = (loacation) => {

  //   setRegion(loacation)
  //   GLOBALS.mapLng = loacation.longitude;
  //   GLOBALS.mapLat = loacation.latitude
  //   setLat(loacation.longitude)
  //   setLng(loacation.latitude)
   }
   const saveFavArea = () => {
  //   CommonApi.fetchAppCommon('User/user-favouritespot-latitude-longitude?id=' + store.response.userModel.id + '&userFavouriteLat=' + GLOBALS.mapLat + '&userFavouriteLong=' + GLOBALS.mapLng, 'PUT', '', store.response.token.token).then(
  //     res => {
  //       console.log(JSON.stringify(res))
  //       // navigation.navigate('favourite_spot')
  //     }).catch(err => {
  //     })
  //   navigation.navigate('favourite_spot')
   
  }

  return (
    <SafeAreaView style={appStyles.container}>
      {loading ? <View style={appStyles.loader}>
        <AnimatedLoader
          visible={loading}
          // source={require("../assets/loader.json")}
          animationStyle={{ height: 70, width: 70 }}
          speed={1}
        />
      </View> : null}
      <StatusBar backgroundColor={'#fafafa'} barStyle='dark-content' />
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={[appStyles.navBar, { backgroundColor: 'transparent' }]}>
          <View style={[appStyles.leftContainer, { backgroundColor: 'transparent' }]}>
            <TouchableOpacity style={appStyles.leftContainer} onPress={() => { navigation.navigate('sharelocation') }}>
              {/* <Image style={[appStyles.headerBackButton]} source={require('../assets/images/back.png')} /> */}
              <Text style={[appStyles.headerStartText]}>Location</Text>
            </TouchableOpacity>
          </View>
          <View style={[appStyles.hederFont]}>
            <View style={{ height: 6, width: 120, backgroundColor: '#ebebeb', borderRadius: 5 }}>
              <View style={{ height: 6, width: 50, backgroundColor: '#d7d7d7', borderRadius: 5 }}>
              </View>
            </View>
          </View>
          <TouchableOpacity style={appStyles.rightContainer} onPress={() => { navigation.navigate('ShareLocation') }}>
            <Text style={[appStyles.headerskipText]}>Skip</Text>
            <View style={appStyles.rightIcon} />
          </TouchableOpacity>
        </View>
        <View>
          <View style={[appStyles.namenageSubtitle, { marginLeft: 24, marginRight: 24, marginTop: 0, marginBottom: 0, height: '100%', flex: 1, bottom: 0, backgroundColor: '#fafafa', display: 'flex', flexDirection: "column", justifyContent: "space-between" }]}>
            <Animatable.Text animation={'fadeInRight'} duration={800} delay={300} style={[appStyles.screenSubtitle, appStyles.openSansExtraBold, { color: '#404040', marginTop: 20, marginBottom: 0 }]}>Favourite area</Animatable.Text>
            <Animatable.Text animation={'fadeInRight'} duration={800} delay={400} style={[appStyles.txtName, appStyles.openSansRegular, { marginTop: 1, top: 0, left: 0, marginBottom: 15, fontFamily: 'greycliffcf-demibold' }]}>Drag the map your favourite area</Animatable.Text>
          </View>
          <Animatable.View animation={'fadeInRight'} duration={800} delay={500}>

            {mapInsize ?
              <View style={{  }}>
                <MapView
                  showsUserLocation={true}
                  zoomEnabled={true}
                  style={[appStyles.mapStyle, { height: screenHeight-60 }]}
                  initialRegion={region}
                  pitch={10}
                  onRegionChangeComplete={onRegionChange}
                >
                  {/* <MapView.Circle
                    key={(lati + lng).toString()}
                    center={{
                      latitude: lati,
                      longitude: lng,
                    }}
                    radius={5000}
                    // strokeWidth={2}
                    strokeColor="#1a66ff"
                    fillColor="#80bfff"
                    strokeWidth={10}
                  /> */}
                  <MapView.Circle
                key = { (lati + lng).toString() }
                center={{
                  latitude:lati,
                  longitude:lng,
                }}
                radius = { 20000 }
               strokeWidth = { 1 }
                strokeColor = { '#1a66ff' }
                fillColor = { 'rgba(230,238,255,0.5)' }
        />

                </MapView>
                <View style={{paddingRight:24,paddingLeft:24,position:'absolute',bottom:110,width:'100%'}}>
                <TouchableOpacity style={[appStyles.btnAccpetItem, {  }]} onPress={saveFavArea}>
            <Text style={{ fontSize: 20, color: 'white', fontFamily: 'greycliffcf-bold' }}>Select this area</Text>
          </TouchableOpacity>
          </View>
              </View>
              : null}
          </Animatable.View>
          {/* <TouchableOpacity style={[appStyles.btnAccpetItem, { position: 'absolute', bottom: 0, marginBottom: 0 }]} onPress={saveFavArea}>
            <Text style={{ fontSize: 20, color: 'white', fontFamily: 'greycliffcf-bold' }}>Select this area</Text>
          </TouchableOpacity> */}
         
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  map: {
    flex: 1
  },
  markerFixed: {
    left: '50%',
    marginLeft: -24,
    marginTop: -48,
    position: 'absolute',
    top: '50%'
  },
  marker: {
    height: 48,
    width: 48
  },
  footer: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    bottom: 0,
    position: 'absolute',
    width: '100%'
  },
  region: {
    color: '#fff',
    lineHeight: 20,
    margin: 20
  }
})