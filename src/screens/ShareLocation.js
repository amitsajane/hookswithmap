/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useState, useEffect,useRef} from 'react';
import {
  View,
  Text,
  AppState,
  Image,
  PermissionsAndroid,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  StatusBar,
} from 'react-native';

import appStyles from './appStyles';
// the variable name is arbitrary since it's exported as default
import {useNavigation} from '@react-navigation/native';
import * as Animatable from 'react-native-animatable';
import AnimatedLoader from 'react-native-animated-loader';
import Geolocation from '@react-native-community/geolocation';

export default function ShareLocation() {
  /* variable declartion...*/

  const navigation = useNavigation();
  const [loading, showLoader] = useState(false);
  const [permisson, setPermisson] = useState(PermissionsAndroid.RESULTS.GRANTED);
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  useEffect(() => {
    
    AppState.addEventListener('change', _handleAppStateChange);

    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
    };
  }, []);

  //getting the Longitude from the location

  const allowLocation = () => {
    Geolocation.getCurrentPosition(
      (info) => {
        const long = info.coords.longitude;
        const lat = info.coords.latitude;
        var initialRegion = {
          latitude: 0,
          longitude: 0,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        };
       latitude = lat;
        lngtude = long;
      },
      (error) => console.log('aa'),
      {enableHighAccuracy: true, timeout: 20000},
    );
    showLoader(true);
    setTimeout(() => {
      // write your functions
      showLoader(false);
      navigation.navigate('FavouriteArea');
    }, 200);
  };

  async function requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        navigation.navigate('FavouriteArea');
        console.log('You can use the location');
        // alert("You can use the location");
      } else {
        setPermisson(!permisson)
        // navigation.navigate('locationScreen');
        console.log('location permission denied');
        // alert('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  const _handleAppStateChange = (nextAppState) => {
    if (appState.current.match(/inactive|background/) &&
      nextAppState === 'active' &&  setPermisson(permisson)  ) {
        // permisson(setPermisson)
      console.log('permission allowed')
      console.log('granted', PermissionsAndroid.RESULTS)
        setPermisson(PermissionsAndroid.RESULTS.GRANTED)
      
      console.log('App has come to the foreground!');
    }
    else {
      // alert("jjjjj")
    }

    appState.current = nextAppState;
    setAppStateVisible(appState.current);
    console.log('AppState', appState.current);
  };

  return (
    <SafeAreaView style={appStyles.container}>
      <StatusBar backgroundColor={'#fafafa'} barStyle="dark-content" />

      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        {loading ? (
          <View style={appStyles.loader}>
            <AnimatedLoader
              visible={loading}
              // source={require("../assets/loader.json")}
              animationStyle={{height: 70, width: 70}}
              speed={1}
            />
          </View>
        ) : null}
       
        <View style={[appStyles.navBar, {backgroundColor: 'transparent'}]}>
          <View
            style={[
              appStyles.leftContainer,
              {backgroundColor: 'transparent', padding: 0},
            ]}>
            <TouchableOpacity
              style={appStyles.leftContainer}
              onPress={() => {
                navigation.navigate('lookingFor');
              }}>
              {/* 
                            <Image style={[appStyles.headerBackButton]}
                                source={require('../assets/images/back.png')} /> */}
              <Text style={[appStyles.headerStartText]}>Preference</Text>
            </TouchableOpacity>
          </View>
          <View style={[appStyles.hederFont, {left: 5}]}>
            <View
              style={{
                height: 6,
                width: 120,
                backgroundColor: '#ebebeb',
                borderRadius: 5,
              }}>
              <View
                style={{
                  height: 6,
                  width: 50,
                  backgroundColor: '#d7d7d7',
                  borderRadius: 5,
                }}></View>
            </View>
          </View>
          <View style={appStyles.rightContainer}>
            <View style={appStyles.rightIcon} />
          </View>
        </View>
        {permisson ?
        <View
          style={[
            appStyles.namenageSubtitle,
            {
              marginLeft: 24,
              marginRight: 24,
              marginTop: 0,
              marginBottom: 0,
              height: '100%',
              flex: 1,
              bottom: 0,
              backgroundColor: '#fafafa',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between',
            },
          ]}>
          <Animatable.View
            animation={'zoomIn'}
            // eslint-disable-next-line prettier/prettier
            duration={800}
            // eslint-disable-next-line prettier/prettier
            delay={600}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              top: '23%',
            }}>
            {/* <Image style={{ height: 80, width: 80, tintColor: '#FC8D8D' }} source={require('../assets/images/marker.png')} /> */}
            <View style={{top: 24}}>
              <Text
                style={[appStyles.screenSubtitle, appStyles.openSansExtraBold]}>
                Share location
              </Text>
            </View>
            <Text
              style={{
                fontSize: 18,
                textAlign: 'center',
                top: 30,
                fontFamily: 'greycliffcf-demibold',
                color: '#404040',
              }}>
              In order to find people in your area we need to see your location.
            </Text>
          </Animatable.View>
          <Animatable.View animation={'fadeInUp'} duration={800} delay={300}>
            <TouchableOpacity
              style={[appStyles.btnAccpetItem, {}]}
              onPress={requestLocationPermission}>
              <Text
                style={{
                  fontSize: 20,
                  color: 'white',
                  fontFamily: 'greycliffcf-bold',
                }}>
                Allow
              </Text>
            </TouchableOpacity>
          </Animatable.View>
        </View>:
         <View
         style={{
           width: '100%',
           flex: 1,
           alignItems: 'center',
           justifyContent: 'center',
           padding: 10,
         }}>
         <Image
           source={{
             uri:
               'https://raw.githubusercontent.com/AboutReact/sampleresource/master/location.png',
           }}
           style={{width: 100, height: 100, marginTop: 30}}
         />
         <Text
           style={{
             fontSize: 25,
             color: '#111111',
             fontWeight: 'bold',
             marginTop: 22,
           }}>
           Oh no!
         </Text>
         <Text
           style={{
             fontSize: 20,
             color: '#404040',
             fontWeight: '800',
             textAlign: 'center',
             marginTop: 10,
           }}>
           Zazu can't be used without allowing location services
         </Text>
         <Text
           style={{
             fontSize: 20,
             color: '#404040',
             fontWeight: '800',
             textAlign: 'center',
             marginTop: 10,
           }}>
           Go to You phones:
         </Text>
         <Text
           style={{
             fontSize: 20,
             color: '#404040',
             fontWeight: '800',
             textAlign: 'center',
             marginTop: 10,
           }}>
           Settings-> Zazu -> Location and chose allow
         </Text>
       </View>
         }

      </ScrollView>
    </SafeAreaView>
  );
}
